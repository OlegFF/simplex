<?php
require $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include.php';
require_once( $_SERVER['REAL_FILE_PATH'].'../../tagtable.php');
use Simplex\TagsTable;

if($_REQUEST['numnews'] > 0){
    $rs = TagsTable::query()
        ->addSelect('*')
        ->addFilter('=ID_NEWS', $_REQUEST['numnews'])
        ->exec()->FetchAll();
}

?>


<script src="//cdn.jsdelivr.net/npm/babel-polyfill/dist/polyfill.min.js"></script>
<script src="//cdn.jsdelivr.net/npm/vue@2.x/dist/vue.js"></script>
<script src="//cdn.jsdelivr.net/npm/vuetify@2.2.8/dist/vuetify.min.js"></script>

<link crossorigin="anonymous" media="all"  rel="stylesheet" href="//fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" />
<link crossorigin="anonymous" media="all"  rel="stylesheet" href="//cdn.jsdelivr.net/npm/@mdi/font@4.x/css/materialdesignicons.min.css" />
<link crossorigin="anonymous" media="all"  rel="stylesheet" href="//fonts.googleapis.com/css?family=Material+Icons" />
<link crossorigin="anonymous" media="all"  rel="stylesheet" href="//cdn.jsdelivr.net/npm/vuetify@2.2.8/dist/vuetify.min.css" />


<div id="app">
    <v-app id="inspire">
        <v-data-table
            :headers="headers"
            :items="tagsss"
            sort-by="tags"
            class="elevation-1"
        >
            <template v-slot:top>
                <v-toolbar flat color="white">
                    <v-toolbar-title>Сипсок тегов новости</v-toolbar-title>
                    <v-divider
                        class="mx-4"
                        inset
                        vertical
                    ></v-divider>
                    <v-spacer></v-spacer>
                    <v-dialog v-model="dialog" max-width="500px">
                        <template v-slot:activator="{ on }">
                            <v-btn color="primary" dark class="mb-2" v-on="on">Добавить тег</v-btn>
                        </template>
                        <v-card>
                            <v-card-title>
                                <span class="headline">{{ formTitle }}</span>
                            </v-card-title>

                            <v-card-text>
                                <v-container>
                                    <v-row>
                                        <v-col cols="12" sm="6" md="4">
                                            <v-text-field v-model="editedItem.NAME" label="Название"></v-text-field>
                                        </v-col>
                                        <v-col cols="12" sm="6" md="4">
                                            <v-text-field v-model="editedItem.ID" label="Название"></v-text-field>
                                        </v-col>
                                        <v-col cols="12" sm="6" md="4">
                                            <v-text-field v-model="editedItem.ID_NEWS" label="Теги"></v-text-field>
                                        </v-col>
                                    </v-row>
                                </v-container>
                            </v-card-text>

                            <v-card-actions>
                                <v-spacer></v-spacer>
                                <v-btn color="blue darken-1" text @click="close">Cancel</v-btn>
                                <v-btn color="blue darken-1" text @click="save">Save</v-btn>
                            </v-card-actions>
                        </v-card>
                    </v-dialog>
                </v-toolbar>
            </template>
            <template v-slot:item.action="{ item }">
                <v-icon
                    small
                    class="mr-2"
                    @click="editItem(item)"
                >
                    edit
                </v-icon>
                <v-icon
                    small
                    @click="deleteItem(item)"
                >
                    delete
                </v-icon>
            </template>
            <template v-slot:no-data>
                <v-btn color="primary" @click="initialize">Reset</v-btn>
            </template>
        </v-data-table>
    </v-app>
</div>

<script>
    new Vue({
        el: '#app',
        vuetify: new Vuetify(),
        data: () => ({
            dialog: false,
            headers: [
                {
                    text: 'Название',
                    align: 'left',
                    sortable: false,
                    value: 'NAME',
                },
                { text: 'ID тега', value: 'ID' },
                { text: 'ID новости', value: 'ID_NEWS' },
                { text: 'Действия', value: 'action', sortable: false },
            ],
            tagsss: <?=CUtil::PhpToJSObject($rs)?>,
            editedIndex: -1,
            editedItem: {
                ID_NEWS: '',
                NAME: '',
            },
            defaultItem: {
                ID_NEWS: '2020.02.01',
                NAME: '',
            },
        }),

        computed: {
            formTitle () {
                return this.editedIndex === -1 ? 'Новый тег' : 'Редактировать тег'
            },
        },

        watch: {
            dialog (val) {
                val || this.close()
            },
        },

        created () {
            this.initialize()
        },

        methods: {
            initialize () {
            },

            editItem (item) {
                this.editedIndex = this.tagsss.indexOf(item)
                this.editedItem = Object.assign({}, item)
                this.dialog = true
            },

            deleteItem (item) {
                const index = this.tagsss.indexOf(item)
                confirm('Хотите удалить тег?') && this.tagsss.splice(index, 1)
            },

            close () {
                this.dialog = false
                setTimeout(() => {
                    this.editedItem = Object.assign({}, this.defaultItem)
                    this.editedIndex = -1
                }, 300)
            },

            save () {
                if (this.editedIndex > -1) {
                    Object.assign(this.tagsss[this.editedIndex], this.editedItem)
                } else {
                    this.tagsss.push(this.editedItem)
                }
                this.close()
            },
        },
    })
</script>