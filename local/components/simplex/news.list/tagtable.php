<?php

/**
 * Created by PhpStorm.
 * User: Oleg.Fahrutdinov
 */

namespace Simplex;

use Bitrix\Main,
    Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

/**
 * Class TagsTable
 *
 * Fields:
 * <ul>
 * <li> ID int mandatory
 * <li> ID_NEWS int optional
 * <li> NAME string(250) mandatory
 * </ul>
 *
 * @package Bitrix\Tags
 **/

class TagsTable extends Main\Entity\DataManager
{
    /**
     * Returns DB table name for entity.
     *
     * @return string
     */
    public static function getTableName()
    {
        return 'simplex_tags';
    }

    /**
     * Returns entity map definition.
     *
     * @return array
     */
    public static function getMap()
    {
        return array(
            'ID' => array(
                'data_type' => 'integer',
                'primary' => true,
                'autocomplete' => true,
                'title' => Loc::getMessage('TAGS_ENTITY_ID_FIELD'),
            ),
            'ID_NEWS' => array(
                'data_type' => 'integer',
                'title' => Loc::getMessage('TAGS_ENTITY_ID_NEWS_FIELD'),
            ),
            'NAME' => array(
                'data_type' => 'string',
                'required' => true,
                'validation' => array(__CLASS__, 'validateName'),
                'title' => Loc::getMessage('TAGS_ENTITY_NAME_FIELD'),
            ),
        );
    }
    /**
     * Returns validators for NAME field.
     *
     * @return array
     */
    public static function validateName()
    {
        return array(
            new Main\Entity\Validator\Length(null, 250),
        );
    }
}