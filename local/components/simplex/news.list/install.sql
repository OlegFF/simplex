-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия сервера:               5.7.25 - MySQL Community Server (GPL)
-- Операционная система:         Win64
-- HeidiSQL Версия:              10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Дамп структуры для таблица sitemanager.simplex_tags
DROP TABLE IF EXISTS `simplex_tags`;
CREATE TABLE IF NOT EXISTS `simplex_tags` (
  `ID` int(18) NOT NULL AUTO_INCREMENT,
  `ID_NEWS` int(11) DEFAULT NULL,
  `NAME` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Дамп данных таблицы sitemanager.simplex_tags: ~0 rows (приблизительно)
DELETE FROM `simplex_tags`;
/*!40000 ALTER TABLE `simplex_tags` DISABLE KEYS */;
INSERT INTO `simplex_tags` (`ID`, `ID_NEWS`, `NAME`) VALUES
	(1, 1, 'tag11'),
	(2, 2, 'tag21'),
	(3, 3, 'tag31'),
	(4, 4, 'tag41'),
	(5, 5, 'tag51'),
	(6, 6, 'tag61'),
	(7, 7, 'tag71'),
	(8, 8, 'tag81'),
	(9, 9, 'tag91'),
	(10, 10, 'tag101'),
	(11, 11, 'tag111'),
	(12, 12, 'tag121'),
	(13, 13, 'tag131'),
	(14, 14, 'tag141'),
	(15, 15, 'tag151'),
	(16, 16, 'tag161'),
	(17, 17, 'tag171'),
	(18, 18, 'tag181'),
	(19, 19, 'tag191'),
	(20, 20, 'tag201'),
	(21, 21, 'tag211'),
	(22, 22, 'tag221'),
	(23, 23, 'tag231'),
	(24, 24, 'tag241'),
	(25, 25, 'tag251'),
	(26, 26, 'tag261'),
	(27, 27, 'tag271'),
	(28, 28, 'tag281'),
	(29, 29, 'tag291'),
	(30, 30, 'tag301'),
	(31, 31, 'tag311'),
	(32, 32, 'tag321'),
	(33, 33, 'tag331'),
	(34, 34, 'tag341'),
	(35, 35, 'tag351'),
	(36, 36, 'tag361'),
	(37, 37, 'tag371'),
	(38, 38, 'tag381'),
	(39, 39, 'tag391'),
	(40, 40, 'tag401'),
	(41, 41, 'tag411'),
	(42, 42, 'tag421'),
	(43, 43, 'tag431'),
	(44, 44, 'tag441'),
	(45, 45, 'tag451'),
	(46, 46, 'tag461'),
	(47, 47, 'tag471'),
	(48, 48, 'tag481'),
	(49, 49, 'tag491'),
	(50, 50, 'tag501');
/*!40000 ALTER TABLE `simplex_tags` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
